#include "interface.h"
#include "carta.h"
#include <QDebug>
#include<cmath>

Interface::Interface(QWidget *parent) :
    QWidget(parent) {
   setupUi(this);
   QPixmap backgnd (":/pic/picture/more.jpg");
   backgnd =backgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
   QPalette palette;
   palette.setBrush(QPalette::Background, backgnd);
   this->setPalette(palette);
   dt = 5;
   timer = new QTimer();
   connect(wgtcarta->btnStartDv,SIGNAL(clicked(bool)), SLOT(startTimer(bool)));
   connect(timer, SIGNAL(timeout()), SLOT(tick()));
   connect(this, SIGNAL(setXY(double,double)),wgtcarta->wgtMapForm, SLOT(setXY(double,double)));

}


Interface::~Interface()
{

}
void Interface::startTimer(bool status)
{
    if (status){

        timer->start(dt);

    }
    else timer->stop();
    qDebug()<<status;
}


void Interface::tick()
{
   double vr;
    vr=wgtcarta->wgtMapForm->getr();
    double psi;
    psi=wgtcarta->wgtMapForm->getf();
    double dr=vr;
    double xp;
    xp=wgtcarta->wgtMapForm->getXp();
    double yp;
    yp=wgtcarta->wgtMapForm->getYp();
    double V=2;
    bool status;
   // r=0;
    if ( r < vr)
       {
        r=sqrt(X*X+Y*Y);
        X+=dt*0.01*V*cos(psi/57.3);
        Y+=dt*0.01*V*sin(psi/57.3);

        emit setXY(X,Y);
       // dr=vr-r;
    } else
    {status=false;
    void startTimer(bool);}
//timer->stop();
    //код, который вызывается по таймеру
    qDebug()<<vr<<" "<<r<<" "<<dr;
}

#ifndef MAPFORM_H
#define MAPFORM_H

#include <QWidget>
#include "ui_mapform.h"
#include <QtCharts>
#include <QHBoxLayout>
#include <cmath>
#include <QMouseEvent>
#include <QDebug>
<<<<<<< HEAD
#include <QtGui>
=======
>>>>>>> f3afc5e62c06c95c57cbfc6915df78af8e7eec65


using namespace QtCharts;

class mapform : public QWidget, public Ui::mapform
{
    Q_OBJECT
private:
    QChartView *chartView;
private:


public:
    explicit mapform(QWidget *parent = 0);
    ~mapform();
    double getr(){return r;}
    double getf(){return f;}
    double getXp(){return Xp;}
    double getYp(){return Yp;}
protected:
   void mousePressEvent(QMouseEvent *e);
    //void mouseDoubleClickEvent(QMouseEvent *e);
 signals:
   void setTabValues (double xk, double yk);
   void setRU (double r, double f);
public slots:
   void clearAll();
    void setXY (double Xp, double Yp);
    void scalePlus ();
    void scaleMinus ();
    void moveUp();
    void moveDown ();
    void moveLeft ();
    void moveRight();
    void scaleReset ();
    void setABC(double X, double Y);
private:
    QHBoxLayout* hlay;
    QChart* chart;
    QSplineSeries* splineSeries;
    QValueAxis* xAxis;
    QValueAxis* yAxis;
    QScatterSeries *scatterSeries, *series1;
    double X,Y;
    double Xp, Yp;
    double Xc, Yc;
    double f, r;
    double xk, yk;
};

#endif // MAPFORM_H

#include "mapform.h"



using namespace std;
mapform::mapform(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    r=0;
    f=0;
//    xk=0;
//    yk=0;
        connect (this, SIGNAL (setTabValues(double,double)), this, SLOT(setABC(double,double)));
//        chart->setBackground("/pic/picture/more.jpg");

        //chart->setBackgroundBrush(bkgnd);
        splineSeries = new QSplineSeries;
        scatterSeries= new QScatterSeries();


        QPainter PAA;
        QImage pa(":/pic/picture/pa.png");
         //painter.drawPath(PAA);
        series1 = new QScatterSeries();
        series1->setName ("Цель");
        series1-> setMarkerShape(QScatterSeries::MarkerShapeCircle);
        series1->setMarkerSize (10.0);


        xAxis = new QValueAxis;
        yAxis = new QValueAxis;
        chart = new QChart;
        scatterSeries->setMarkerShape(QScatterSeries::MarkerShapeRectangle);
       scatterSeries->setMarkerSize(50.0);
        scatterSeries->setBrush(pa);
        chart->setBackgroundVisible(false);
        chart->addSeries(splineSeries);
        chart->addSeries(scatterSeries);
        chart->addSeries(series1);
        chart->legend()->setVisible(false);
       // splineSeries->setColor(Qt::red);


        chart->setAxisX(xAxis,splineSeries);
        chart->setAxisY(yAxis,splineSeries);
        chart->setAxisX(xAxis,scatterSeries);
        chart->setAxisY(yAxis,scatterSeries);
        chart->setAxisX(xAxis,series1);
        chart->setAxisY(yAxis,series1);

        chartView = new QChartView(this);

        hlay = new QHBoxLayout();
        hlay->addWidget(chartView);
        setLayout(hlay);
        chartView->setRenderHint(QPainter::Antialiasing);
        chartView->setChart(chart);


        xAxis->setRange(-100,100);
        yAxis->setRange(-100,100);
        xAxis->setLabelsColor(Qt::black);
        yAxis->setLabelsColor(Qt::black);

        xAxis->setTickCount(11);
        yAxis->setTickCount(11);

        xAxis->setTitleText("X, м");
        xAxis->setTitleText("Y, м");

        splineSeries->append(0,0);
        scatterSeries ->append(0,0);


}

mapform::~mapform()
{

}

void mapform::setXY(double X, double Y) {

    splineSeries->append(X,Y);
    scatterSeries->clear();
    scatterSeries->append(X,Y);
    this->Xp = X;
    this->Yp = Y;
 }
void mapform::moveRight()
{
    chart->scroll(10,0);
}
void mapform::moveLeft()
{
    chart->scroll(-10,0);

}
void mapform::scalePlus(){
    chart->zoomIn();
}

void mapform::scaleMinus(){
    chartView->chart()->zoomOut();

}
void mapform::moveUp(){
    chart->scroll(0,10);

}

void mapform::moveDown(){
    chartView->chart()->scroll(0,-10);
}
void mapform::scaleReset(){
    chart->zoomReset();
}

void mapform::mousePressEvent(QMouseEvent *e)
{

   double x=e->localPos().x()-this->width()/2;
   double y=e->localPos().y()-this->height()/2;
    xk=x/2.35;
    yk=-y/1.95;
    emit setTabValues(xk,yk);
       r=sqrt((Xp-xk)*(Xp-xk)+(Yp-yk)*(Yp-yk));
        if(x!=0)
        {
            f=-atan2(y,x)*57.3;
        }
        emit setRU(r,f);
}

void mapform::clearAll()
{
    series1->clear();
}
void mapform::setABC(double X, double Y)
{


    series1->append(X,Y);


}


#ifndef INTERFACE_H
#define INTERFACE_H

#include <QWidget>
#include <QTimer>
#include "ui_interface.h"
#include "carta.h"

class Interface : public QWidget, public Ui::Interface
{
    Q_OBJECT
public:
    explicit Interface(QWidget *parent = 0);
    ~Interface();
    int dt; //период тика таймера

    QTimer *timer;
private:
    double X, Y;
    double r,vr;
public slots:
    void tick();
    void startTimer(bool);
signals:
    void setXY (double, double);
};

#endif // INTERFACE_H

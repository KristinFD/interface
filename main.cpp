#include "interface.h"
#include "carta.h"
#include "mapform.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Interface w;
    w.setWindowTitle("Работа с картой");
    w.show();

    return a.exec();
}

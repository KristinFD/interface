#ifndef CARTA_H
#define CARTA_H

#include <QWidget>

#include "ui_carta.h"
#include "mapform.h"
#include <QTableWidget>
#include <QEvent>


class carta : public QWidget, public Ui::wgtCarta
{
    Q_OBJECT

public:
    explicit carta(QWidget *parent = 0);
    ~carta();


private:
    int dt;
//    QTimer *time;
    double X,Y,V,psi;
  double rast, ugol;
public slots:
  //  void startTimer(bool);
   // void tick();
  void setTabValues (double, double);
  void setRU (double, double);

signals:
    void setXY(double X, double Y);

};

#endif // CARTA_H
